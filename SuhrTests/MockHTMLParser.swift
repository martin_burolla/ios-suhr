//
//  MockHTMLParser.swift
//  Suhr
//
//  Created by Marty Burolla on 5/8/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class MockHTMLParser : HTMLParserProtocol
{
    // MARK: - Data members
    
    var guitars: [Guitar] = []
    var delegate: HTMLParserDelegate! = nil
    
    let suhrProxyProtocol: SuhrProxyProtocol?

    // MARK: - Constructors
    
    init(suhrProxyProtocol: SuhrProxyProtocol)
    {
        self.suhrProxyProtocol = suhrProxyProtocol
    }
    
    func getJustShippedGuitarsAndCacheImages()
    {
        self.suhrProxyProtocol?.getJustShippedHTMLwithCompletion({ (html) -> Void in
            
            // Production code parses the html and creates guitar objects.
            
            let mockGuitar = Guitar()
            mockGuitar.model = "Mock Model"
            mockGuitar.imageURL = "Mock image URL"
            mockGuitar.msrp = "Mock MSRP"
            mockGuitar.neckProfile = "Mock neck profile"
            
            let dealer = Dealer()
            dealer.name = "Mock dealer name"
            dealer.url = "Mock dealer URL"
            
            self.guitars.append(mockGuitar)
            
            self.delegate?.didFinishParsingOneGuitar(self, guitarCount: 1)
            self.delegate?.didFinishParsing(self)
            
            // I'm just a mock, so I don't cache images.
            //self.suhrProxyProtocol?.getImagesAndStoreIntoCache(mockGuitar)
        })
    }
}