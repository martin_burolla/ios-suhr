//
//  MockSuhrProxy.swift
//  Suhr
//
//  Created by Marty Burolla on 5/8/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import Alamofire

class MockSuhrProxy : SuhrProxyProtocol
{
    func getJustShippedHTMLwithCompletion(completion: ((html: String) -> Void))
    {
        // Production code uses the Suhr website to get html data.
        completion(html: "<html>Just Shipped</html>")
    }
    
    func getSpecSheetHTMLForGuitarWithCompletion(guitar : Guitar, completion: ((html: String) -> Void))
    {
        // Production code uses the Suhr website to get html data.
        completion(html: "<html>Spec Sheet</html>")
    }
    
    func getImagesAndStoreIntoCache(guitar: Guitar)
    {
        let sharedImageCache: ImageCache = ImageCache.sharedImageCache
        
        let downloadQueue = dispatch_queue_create("suhrdownloadqueue",nil)
        
        dispatch_async(downloadQueue)
            {
                let cacheGuitarImage = CacheGuitarImage()
                
                // Get lo-res image.
                let loResImageData = NSData(contentsOfURL: NSURL(string: guitar.imageURL!)!)
                if (loResImageData != nil) {
                    cacheGuitarImage.loResImage = UIImage(data: loResImageData!)
                }
                
                // Get hi-res image
                let hiResImageData = NSData(contentsOfURL: NSURL(string: guitar.largeImageURL!)!)
                if (hiResImageData != nil) {
                    cacheGuitarImage.hiResImage = UIImage(data: hiResImageData!)
                }
                
                dispatch_async(dispatch_get_main_queue())
                {
                    sharedImageCache.guitarImages[guitar.imageURL!] = cacheGuitarImage
                }
        }
    }
}