//
//  MockUserPreferenceProxy.swift
//  Suhr
//
//  Created by Marty Burolla on 5/7/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class MockUserPreferenceProxy : UserPreferenceProtocol
{
    // MARK: - Data Members
    
    var mockUserDefaults = UserDefaultSettings()
    
    init()
    {
        self.mockUserDefaults.lastViewedGuitarImageURL = ""
        self.mockUserDefaults.isFilterOn = false
        self.mockUserDefaults.filterThisModel = "Modern"
    }
    
    // MARK: - Methods
    
    func readUserDefaults() -> UserDefaultSettings
    {
        return self.mockUserDefaults
    }
    
    func writeUserDefaults(userDefaults: UserDefaultSettings)
    {
        self.mockUserDefaults = userDefaults
    }
}