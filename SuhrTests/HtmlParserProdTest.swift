//
//  SuhrTests.swift
//  SuhrTests
//
//  Created by Marty Burolla on 4/28/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import XCTest

class HtmlParserProdTest : XCTestCase, HTMLParserDelegate
{
    // MARK: - Data members
    
    let kTimeoutInSeconds = 40.0
    var expectation: XCTestExpectation?
    var parsedGuitarCount = 0
    
    var htmlParserProtocol: HTMLParserProtocol?
    
    // MARK: - Life Cycle
    
    override func setUp()
    {
        super.setUp()
        self.expectation = expectationWithDescription("Get guitars with HTML Parser")
        
        // Inject our dependencies:
        //   Production HTML Parser
        //   Production Suhr proxy
        self.htmlParserProtocol = HTMLParser(suhrProxyProtocol: SuhrProxy())
        self.htmlParserProtocol!.delegate = self
        self.htmlParserProtocol!.getJustShippedGuitarsAndCacheImages()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // MARK: - Test Methods
    
    func test_HTMLParserGet50Guitars()
    {
        waitForExpectationsWithTimeout(kTimeoutInSeconds, handler: { (error) -> Void in
            XCTAssert(self.htmlParserProtocol!.guitars.count == 50, "Did not parse 50 guitars from HTML.")
            XCTAssert(self.parsedGuitarCount == 50)
        })
    }
    
    // MARK: - SuhrNewGuitarFeedParserDelegate
    
    func didFinishParsing(suhrNewGuitarFeedParser: HTMLParserProtocol)
    {
        self.expectation!.fulfill()
    }
    
    func didFinishParsingOneGuitar(suhrNewGuitarFeedParser: HTMLParserProtocol, guitarCount: Int)
    {
        self.parsedGuitarCount = guitarCount
    }
}