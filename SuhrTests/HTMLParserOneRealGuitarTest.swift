//
//  HTMLParserOneRealGuitarTest.swift
//  Suhr
//
//  Created by Marty Burolla on 5/8/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import XCTest

class HtmlParserOneRealGuitarTest : XCTestCase, HTMLParserDelegate
{
    // MARK: - Data members
    
    let kTimeoutInSeconds = 40.0
    var expectation: XCTestExpectation?
    var parsedGuitarCount = 0
    
    var htmlParserProtocol: HTMLParserProtocol?
    
    // MARK: - Life Cycle
    
    override func setUp()
    {
        super.setUp()
        self.expectation = expectationWithDescription("Get guitars with HTML Parser")
        
        // Inject our dependencies:
        //   Mock HTML Parser
        //   Mock Suhr proxy
        self.htmlParserProtocol = MockHTMLParser_OneRealGuitar(suhrProxyProtocol: MockSuhrProxy())
        self.htmlParserProtocol!.delegate = self
        self.htmlParserProtocol!.getJustShippedGuitarsAndCacheImages()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // MARK: - Test Methods
    
    func test_HTMLParserGet50Guitars()
    {
        waitForExpectationsWithTimeout(kTimeoutInSeconds, handler: { (error) -> Void in
          
            XCTAssert(self.htmlParserProtocol?.guitars[0].model == "Standard")
        })
    }
    
    // MARK: - SuhrNewGuitarFeedParserDelegate
    
    func didFinishParsing(suhrNewGuitarFeedParser: HTMLParserProtocol)
    {
        self.expectation!.fulfill()
    }
    
    func didFinishParsingOneGuitar(suhrNewGuitarFeedParser: HTMLParserProtocol, guitarCount: Int)
    {
        self.parsedGuitarCount = guitarCount
    }
}