//
//  GuitarManagerTest.swift
//  Suhr
//
//  Created by Marty Burolla on 5/5/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import XCTest

class GuitarManagerFilterTest : XCTestCase, SuhrGuitarManagerDelegate
{
    // MARK: - Data members
    
    var parsedGuitarCount = 0
    let kTimeoutInSeconds = 40.0
    var expectation: XCTestExpectation?
    var mockUserPreferenceProxy: MockUserPreferenceProxy = MockUserPreferenceProxy()
    
    var guitarManager: GuitarManager?
    
    // MARK: - Life Cycle
    
    override func setUp()
    {
        super.setUp()
        self.expectation = expectationWithDescription("Get guitars with GuitarManager")
        
        // Inject our dependencies: 
        //   Production HTML Parser
        //   Production Suhr proxy
        //   Mock UserDefaults
        self.guitarManager = GuitarManager(htmlParserProtocol: HTMLParser(suhrProxyProtocol: SuhrProxy()), userPreferenceProtocol: self.mockUserPreferenceProxy)
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // MARK: - Test Methods
    
    func test_GuitarManagerGetModernGuitars()
    {
        self.guitarManager!.delegate = self
        self.guitarManager!.getJustShippedGuitars()
        
        waitForExpectationsWithTimeout(kTimeoutInSeconds, handler: { (error) -> Void in
            
            // The Guitar Manager holds a subset of the guitars stored in the HTML parser.
            for guitar: Guitar in self.guitarManager!.guitarsToDisplay {
                XCTAssert(guitar.model == "Modern")
            }
            
            // The html parser holds ALL the guitars.
            //XCTAssert(self.guitarManager!.htmlParserProtocol!.guitars.count == 50)
        })
    }
    
    // MARK: - SuhrNewGuitarFeedParserDelegate
    
    func didFinishGettingGuitars(suhrGuitarManager: GuitarManager)
    {
        let onlyShowThisModel = "Modern"
   
        self.guitarManager!.filterGuitars(onlyShowThisModel)
        self.expectation!.fulfill()
    }
    
    func didFinishGettingOneGuitar(suhrGuitarManager: GuitarManager, guitarCount: Int)
    {
        self.parsedGuitarCount = guitarCount
    }
}