//
//  MockHTMLParser_OneRealGuitar.swift
//  Suhr
//
//  Created by Marty Burolla on 5/8/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class MockHTMLParser_OneRealGuitar : HTMLParserProtocol
{
    // MARK: - Data members
    
    var guitars: [Guitar] = []
    var delegate: HTMLParserDelegate! = nil
    
    let suhrProxyProtocol: SuhrProxyProtocol?
    
    // MARK: - Constructors
    
    init(suhrProxyProtocol: SuhrProxyProtocol)
    {
        self.suhrProxyProtocol = suhrProxyProtocol
    }
    
    func getJustShippedGuitarsAndCacheImages()
    {
        self.suhrProxyProtocol?.getJustShippedHTMLwithCompletion({ (html) -> Void in
            
            // Production code parses the html and html spec sheet and creates guitar objects.
            
            // self.suhrProxyProtocol?.getSpecSheetHTMLForGuitarWithCompletion(guitar: Guitar, 
            //     completion: { (html) -> Void in
            //
            //})
            
            self.guitars.removeAll(keepCapacity: true)
            
            let mockGuitar = Guitar()
            mockGuitar.model = "Standard"
            mockGuitar.neckProfile = "10 - 14"
            mockGuitar.detailURL = "http://www.suhr.com//just-shipped-gallery/standard-27033.html"
            mockGuitar.imageURL = "http://www.suhr.com/52074-month-cat-img.jpg"
            mockGuitar.largeImageURL = "http://www.suhr.com/52074-main-img-front.jpg"
            mockGuitar.isNew = true
            mockGuitar.msrp = "5230.00"
          
            let dealer = Dealer()
            dealer.name = "Guitar Emporium"
            dealer.url = "http://www.guitar-emporium.com/m-7-suhr.aspx"
            mockGuitar.dealer = dealer
            self.guitars.append(mockGuitar)
            
            self.suhrProxyProtocol?.getImagesAndStoreIntoCache(mockGuitar)
            
            self.delegate?.didFinishParsingOneGuitar(self, guitarCount: 1)
            self.delegate?.didFinishParsing(self)
        })
    }
}