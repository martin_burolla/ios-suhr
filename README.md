## Just Shipped Suhr Guitars [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56d0ffe70a10690100b3d633&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56d0ffe70a10690100b3d633/build/latest)
Displays the last 50 guitars that were just shipped from Suhr guitars on iPhone and iPad.

<table border=0>
<tr><td><img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-Loading.PNG width=350 height=570 ></td>
<td><img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-Loaded.PNG width=350 height=570 ></td><tr>
</table>

<table border=0>
<tr><td><img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-Guitar-Details.PNG width=350 height=570 ></td>
<td><img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-Filter.PNG width=350 height=570 ></td><tr>
</table>

<table border=0>
<tr><td><img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-just-shipped-push-notifications.PNG/ width=350 height=570 ></td>
</table>


<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-iPad.PNG/>
</p>

<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/Suhr/Suhr-Just-Shipped-Filter-iPad.PNG/>
</p>


### What Problem Does This Solve?

The Suhr website lists the guitars that have just shipped to dealers around the world.  If you're searching for a specific guitar with a particular specification, you must click on every guitar, scroll down and search the spec sheet.  This iOS application displays the picture of a guitar, the model of the guitar and the neck radius of the most recent guitars that have shipped in one simple glance.  

The application also flags guitars as "NEW" if they are being viewed for the first time.  Single tap on a guitar to display the dealer information and the MSRP.  Users have the option of viewing all the guitars, or selecting a specific model.  These features are not available in the "Just Shipped" section on the Suhr website.  The application supports push notifications for just shipped guitars and other messaging.

### Details

  * Builds with Xcode 7.2
  * Written in Swift 1.2
  * Uses CocoaPods, use Suhr.xcworkspace to open the project
  * Uses Parse for push notifications

### External Libraries

 * Snap
 * Alamofire
 * Parse
