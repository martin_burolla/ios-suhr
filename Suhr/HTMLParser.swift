//
//  SuhrNewGuitarFeedParser.swift
//  SuhrNewGuitarFeedParser
//
//  Created by Marty Burolla on 4/21/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

protocol HTMLParserProtocol
{
   var delegate: HTMLParserDelegate! {get set}
   var guitars: [Guitar] {get set}
   
   func getJustShippedGuitarsAndCacheImages()
}

protocol HTMLParserDelegate
{
    func didFinishParsing(suhrNewGuitarFeedParser: HTMLParserProtocol)
    func didFinishParsingOneGuitar(suhrNewGuitarFeedParser: HTMLParserProtocol, guitarCount: Int)
}

class HTMLParser : HTMLParserProtocol
{
    // MARK: - Data Members
    
    let kBaseURL = "http://www.suhr.com/"
    
    var guitars: [Guitar] = [] // The raw unfiltered list of guitars from Suhr.
    var delegate: HTMLParserDelegate! = nil
    
    let suhrProxyProtocol: SuhrProxyProtocol?
    let cacheManager: CacheManager?
    
    // MARK: - Constructors
    
    init(suhrProxyProtocol: SuhrProxyProtocol)
    {
        self.suhrProxyProtocol = suhrProxyProtocol
        self.cacheManager = CacheManager(suhrProxyProtocol: self.suhrProxyProtocol!)
    }
    

    // MARK: - Web calls
    
    func getJustShippedGuitarsAndCacheImages()
    {
        self.suhrProxyProtocol!.getJustShippedHTMLwithCompletion { (html) -> Void in
            
            let serverSideGuitars = self.createGuitarArrayFromJustShippedHTML(html)
         
            if(SyncManager.needToSyncServerSideGuitarsWithLocalGuitars(self.guitars, serverSideGuitars: serverSideGuitars)) {
                self.guitars.removeAll(keepCapacity: true)
                self.guitars = serverSideGuitars
                self.createGuitarDetailsForEachGuitar()
            }
            else {
                self.delegate?.didFinishParsing(self)
            }
        }
    }
    
    private func createGuitarDetailsForEachGuitar()
    {
        var parseGuitarCount = 0
        
        for guitar in self.guitars
        {
            self.suhrProxyProtocol!.getSpecSheetHTMLForGuitarWithCompletion(guitar, completion:
                { (html) -> Void in
                    
                    self.parseSpecSheetFromHTML(html, guitar: guitar)
                    self.cacheManager?.cacheNewGuitarImageFromWebServer(guitar)
                    
                    parseGuitarCount++
                    
                    self.delegate?.didFinishParsingOneGuitar(self, guitarCount: parseGuitarCount)
                    
                    if (parseGuitarCount == self.guitars.count){
                        self.delegate?.didFinishParsing(self)
                    }
            })
        }
    }
    
    // MARK: - Parsing Just Shipped
    
    private func createGuitarArrayFromJustShippedHTML(html: String) -> [Guitar]
    {
        var serverSideGuitars: [Guitar] = []
        let formArray = html.componentsSeparatedByString("<form method='post'")
        
        for htmlForm in formArray
        {
            let guitar = Guitar()
            guitar.imageURL = self.parseImageURL(htmlForm)
            guitar.largeImageURL = self.createLargeImageURL(guitar.imageURL!)
            guitar.detailURL = self.parseDetailURL(htmlForm)
            
            if !(guitar.detailURL!.isEmpty)
            {
                serverSideGuitars.append(guitar)
            }
        }
        
        return serverSideGuitars
    }
    
    private func createLargeImageURL(imageURL: String) -> String
    {
        // Small image: http://www.suhr.com/52074-month-cat-img.jpg
        // Large image: http://www.suhr.com/52074-main-img-front.jpg
        
        return imageURL.componentsSeparatedByString("-")[0] + "-main-img-front.jpg"
    }
    
    private func parseImageURL(htmlForm: String) -> String
    {
        var imageURL: String? = ""
        let rangeIMG = htmlForm.rangeOfString("<img src=")
        let rangeJPG = htmlForm.rangeOfString("jpg")
        
        if(rangeIMG == nil || rangeJPG == nil)
        {
            return imageURL!
        }
        
        if(rangeJPG!.startIndex > rangeIMG?.startIndex)
        {
            let guitarHyperLink = htmlForm.substringWithRange(Range<String.Index>(start: rangeIMG!.startIndex.advancedBy(+11),
                end: rangeJPG!.endIndex))
            imageURL = (self.kBaseURL + guitarHyperLink)
        }
        
        return imageURL!
    }
    
    private func parseDetailURL(htmlForm: String) -> String
    {
        var detailURL: String? = ""
        let rangeAHREF = htmlForm.rangeOfString("<A href=")
        let rangeHTML = htmlForm.rangeOfString("html")
        
        if(rangeAHREF == nil || rangeHTML == nil)
        {
            return detailURL!
        }
        
        if(rangeHTML!.startIndex > rangeAHREF?.startIndex)
        {
            let guitarHyperLink = htmlForm.substringWithRange(Range<String.Index>(start: rangeAHREF!.startIndex.advancedBy(+9),
                end: rangeHTML!.endIndex))
            detailURL = (self.kBaseURL + guitarHyperLink)
        }
        
        return detailURL!
    }
    
    // MARK: - Parsing Spec Sheet
    
    private func parseSpecSheetFromHTML(detailedHTML : String, guitar: Guitar)
    {
        var tdArray: [String] = []
        let rangeSpecSheet = detailedHTML.rangeOfString("<!--Between Tables-->")  //Available from  Spec Sheet
        let rangeMSRP = detailedHTML.rangeOfString("MSRP")
   
        let specSheetHTML = detailedHTML.substringWithRange(Range<String.Index>(start: rangeSpecSheet!.startIndex,
            end: rangeMSRP!.endIndex))
     
        tdArray = specSheetHTML.componentsSeparatedByString("<td")
        guitar.model = self.parseGuitarModel(tdArray)
        guitar.neckProfile = self.parseNeckProfile(tdArray)
        guitar.dealer = self.parseDealer(tdArray)
        guitar.msrp = self.parseMSRP(tdArray)
    }
    
    private func parseMSRP(tdArray : [String]) -> String
    {
        var msrp = ""
        var msrpHTML = ""
        
        for tdHTML: String in tdArray
        {
            if(tdHTML.rangeOfString("MSRP") != nil)
            {
                msrpHTML = tdHTML
                break
            }
        }
        
        if (msrpHTML.characters.count > 0)
        {
            if(msrpHTML.rangeOfString("msrp-price") != nil) {
                let rangeStrongBegin = msrpHTML.rangeOfString("msrp-price")
                let rangeStrongEnd = msrpHTML.rangeOfString("MSRP")
                msrp = msrpHTML.substringWithRange(Range<String.Index>(start: rangeStrongBegin!.startIndex.advancedBy(+13), end: rangeStrongEnd!.endIndex.advancedBy(-5)))
            }
            else {
                let rangeStrongBegin = msrpHTML.rangeOfString("<strong>")
                let rangeStrongEnd = msrpHTML.rangeOfString("</strong>")
                msrp = msrpHTML.substringWithRange(Range<String.Index>(start: rangeStrongBegin!.startIndex.advancedBy(+8), end: rangeStrongEnd!.endIndex.advancedBy(-9)))
            }
        }
        
        return msrp
    }
    
    private func parseDealer(tdArray : [String]) -> Dealer
    {
        let dealer: Dealer =  Dealer()
        var dealerHTML = ""

        for tdHTML: String in tdArray
        {
            if(tdHTML.rangeOfString("fp-2014-dealer") != nil)
            {
                dealerHTML = tdHTML
                break
            }
        }
        
        // Parse Dealer URL
        if (dealerHTML.characters.count > 0)
        {
            // Dealer HTML
            // " align=\"center\">\r\n<div id=\"fp-2014-dealer\"><A href=\"http://www.manisguitarshop.com/collections/elektrische-gitarren/products/suhr-modern-g-bengal-burst-23129\" target=\"_new\">Manis Guitar Shop</A></div><br>\r\n</td>\r\n    </tr>\r\n    <tr>\r\n      "
            let rangeAHREF = dealerHTML.rangeOfString("<A href")
            let rangeA = dealerHTML.rangeOfString("target=")
            dealer.url = dealerHTML.substringWithRange(Range<String.Index>(start: rangeAHREF!.startIndex.advancedBy(+9), end: rangeA!.endIndex.advancedBy(-9)))
            
            let rangeGreater = dealerHTML.rangeOfString("target=")
            let rangeLessthan = dealerHTML.rangeOfString( "</A>")
            
            dealer.name = dealerHTML.substringWithRange(Range<String.Index>(start: rangeGreater!.startIndex.advancedBy(+14), end: rangeLessthan!.endIndex.advancedBy(-4)))
        }
        
        return dealer
    }
    
    private func parseNeckProfile(tdArray : [String]) -> String
    {
        var neckProfile: String = ""
        var index = 0
        
        for tdElement: String in tdArray
        {
            if(tdElement.rangeOfString("Fingerboard Radius:") != nil)
            {
                neckProfile = tdArray[++index]
                break
            }
            ++index
        }
        
        // Clean up neck profile.
        // >10\" - 14\"</td>\r\n      </tr>\r\n      <tr>\r\n        "
        let rangeGreaterThan = neckProfile.rangeOfString(">")
        let rangeTD = neckProfile.rangeOfString("</td>")
        
        neckProfile = neckProfile.substringWithRange(Range<String.Index>(start: rangeGreaterThan!.startIndex.advancedBy(+1),
            end: rangeTD!.endIndex.advancedBy(-5)))
        
        return neckProfile
    }
    
    private func parseGuitarModel(tdArray : [String]) -> String
    {
        var guitarModel: String = ""
        var index = 0
        
        for tdElement: String in tdArray
        {
            if(tdElement.rangeOfString("Guitar Model:") != nil)
            {
                guitarModel = tdArray[++index]
                break
            }
            ++index
        }
        
        // Clean up guitar model text.
        // " width=\"70%\"><A href=\"/standard-style-custom-guitars-by-suhr\">Standard</A></td>\r\n      </tr>\r\n      <tr>\r\n        "
        let rangeAHREF = guitarModel.rangeOfString("<A href")
        let rangeA = guitarModel.rangeOfString("</A>")
        
        let hyperLink = guitarModel.substringWithRange(Range<String.Index>(start: rangeAHREF!.startIndex.advancedBy(+1),
            end: rangeA!.endIndex.advancedBy(-1)))
        
        let rangeGreater = hyperLink.rangeOfString(">")
        let rangeLessthan = hyperLink.rangeOfString("<")

        guitarModel = hyperLink.substringWithRange(Range<String.Index>(start: rangeGreater!.startIndex.advancedBy(+1),
            end: rangeLessthan!.endIndex.advancedBy(-1)))
        
        return guitarModel
    }
}


/*

*** EXAMPLE DATA THAT WE ARE PARSING (4/21/2015) ***

JUST SHIPPED

    <form method='post' name='form51843' id='form51843' action='/app/site/backend/additemtocart.nl?c=3496541&amp;n=1' onsubmit="return checkmandatory51843();">
        <TD style="padding-top: 0px; padding-bottom: 80px;" align="center">
            <TABLE width="100%" border="0" align="left" cellpadding="0" cellspacing="1" bgcolor="#ffffff">
            <TR>
            <TD>
            <TABLE width="100%" border=0 cellspacing=0 cellpadding=0 bgcolor="#FFFFFF">
            <TR>
            <TD width="100%" valign=top>
            <TABLE border=0 cellspacing=0 cellpadding=0 width="100%">
               <TR>
                   <TD height="650" valign="bottom" align="center">
                       <A href="/just-shipped-gallery/modern-26919.html">
                           <img src="/51843-month-cat-img.jpg" style="max-height: 642px;"> <=============================== PARSE THIS TEXT RIGHT HERE.
                       </A>
                   </TD>
               </TR>
            <TR>
            <TD align="center" style="padding-top: 20px;"><span style="font-size: 16px; font-weight: bold; text-transform: uppercase; text-align: center; font-family: Helvetica, Arial, sans-serif;">Modern</span></TD>
            </TR>
            <!--Thin Line between product description and price-->
            </TABLE>
            </TD>
            </TR>
            </TABLE>
            </TD>
            </TR>
            </TABLE>
        </TD>
    </form>


SPEC SHEET

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td align="center">
    <div style="margin-bottom:20px;"><span class="section-header-big"><A name="specs">Spec Sheet</A></span></div>
    <hr>
    </td>
    </tr>
    <tr>
    <td><div align="left" style="font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; text-transform: uppercase;">Body</div>
    <div align="right" style="position:relative; top:-28px;">
    <table width="80%" border="0" cellspacing="0" cellpadding="0" id="GOTW-spec-table">
    <tr>
    <td width="30%">Guitar Model:</td>
    <td width="70%"><A href="/standard-style-custom-guitars-by-suhr">Standard</A></td>
    </tr>
    <tr>
    <td>Top Wood:</td>
    <td><A href="/maple-tone-wood">Spalt Maple, 2-Piece, 3/16"</A></td>
    </tr>
    <tr>
    <td>Body Wood:</td>
    <td><A href="/alder-tone-wood">Alder, 2-Piece</A></td>
    </tr>
    <tr>
    <td>Finish:</td>
    <td>Custom Color</td>
    </tr>
    </table>
    </div>
    <hr></td>
    </tr>
    <tr>
    <td><div align="left" style="font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; text-transform: uppercase;">Neck</div>
    <div align="right" style="position:relative; top:-28px;">
    <table width="80%" border="0" cellspacing="0" cellpadding="0" id="GOTW-spec-table">
    <tr>
    <td width="30%">Neck Wood:</td>
    <td width="70%"><A href="/maple-tone-wood">Maple</A></td>
    </tr>
    <tr>
    <td>Neck Back Shape:</td>
    <td>Even C RB .800 - .850</td>
    </tr>
    <tr>
    <td>Fingerboard Wood:</td>
    <td><A href="/indian-rosewood-tone-wood">Indian Rosewood fingerboard</A></td>
    </tr>
    <tr>
    <td>Fingerboard Radius:</td>
    <td>10" - 14"</td>
    </tr>
    <tr>
    <td width="30%">Frets:</td>
    <td width="70%">22 Frets, Nickel, Heavy</td>
    </tr>
    <tr>
    <td width="30%">Fret Number:</td>
    <td width="70%"></td>
    </tr>
    <tr>
    <td width="30%">Face Dots:</td>
    <td width="70%">Clay Face Dots</td>
    </tr>
    <tr>
    <td width="30%">Side Dots:</td>
    <td width="70%">Clay Side Dots</td>
    </tr>
    <tr>
    <td>Nut:</td>
    <td>Tusq</td>
    </tr>
    <tr>
    <td>Nut Width:</td>
    <td>1.650"</td>
    </tr>
    </table>
    </div>
    <hr></td>
    </tr>
    <tr>
    <td><div align="left" style="font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; text-transform: uppercase;">Electronics</div>
    <div align="right" style="position:relative; top:-28px;">
    <table width="80%" border="0" cellspacing="0" cellpadding="0" id="GOTW-spec-table">
    <tr>
    <td width="30%">Wiring:</td>
    <td width="70%">Standard Wiring</td>
    </tr>
    <tr>
    <td>Input Jack:</td>
    <td>Side Jack, Chrome</td>
    </tr>
    <tr>
    <td>Neck Pickup:</td>
    <td><A href="/ml-pickups">ML Standard, SC, Neck, Black </A></td>
    </tr>
    <tr>
    <td>Middle Pickup:</td>
    <td><A href="/ml-pickups">ML Standard, SC, Middle, Black </A></td>
    </tr>
    <tr>
    <td>Bridge Pickup:</td>
    <td><A href="/aldrich-humbucker-pickup.html">Aldrich, HB, Bridge 53mm, Black </A></td>
    </tr>
    </table>
    </div>
    <hr></td>
    </tr>
    <tr>
    <td><div align="left" style="font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; text-transform: uppercase;">Hardware</div>
    <div align="right" style="position:relative; top:-28px;">
    <table width="80%" border="0" cellspacing="0" cellpadding="0" id="GOTW-spec-table">
    <tr>
    <td width="30%">Pickguard:</td>
    <td width="70%">No Pickguard - Rear Route</td>
    </tr>
    <tr>
    <td>Gears:</td>
    <td>Suhr Locking, Chrome</td>
    </tr>
    <tr>
    <td>Bridge:</td>
    <td>Gotoh 510, 2P-SS-SB, Chrome</td>
    </tr>
    <tr>
    <td>Hardware Color:</td>
    <td>Chrome Hardware</td>
    </tr>
    <tr>
    <td>Knobs:</td>
    <td>Chrome</td>
    </tr>
    </table>
    </div>
    <div>
    </td>
    </tr>
    <tr>
    <td><p>$<strong>4680.00</strong> MSRP</p></td>
    </tr>
    </table>

*/

