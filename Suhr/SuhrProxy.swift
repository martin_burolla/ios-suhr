//
//  SuhrProxy.swift
//  Suhr
//
//  Created by Marty Burolla on 4/27/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import Alamofire

protocol SuhrProxyProtocol
{
    func getJustShippedHTMLwithCompletion(completion: ((html: String) -> Void))
    func getSpecSheetHTMLForGuitarWithCompletion(guitar : Guitar, completion: ((html: String) -> Void))
    func getImagesAndStoreIntoCache(guitar: Guitar)
}

class SuhrProxy : SuhrProxyProtocol
{
    // MARK: - HTML Getters
    
   func getJustShippedHTMLwithCompletion(completion: ((html: String) -> Void))
    {
        Alamofire.request(.GET, "http://www.suhr.com/just-shipped-gallery/", parameters: nil)
            .responseString { (response) in
                completion(html: response.result.value!)
        }
    }
    
    func getSpecSheetHTMLForGuitarWithCompletion(guitar : Guitar, completion: ((html: String) -> Void))
    {
        Alamofire.request(.GET, guitar.detailURL!, parameters: nil)
            .responseString { (response) in
                completion(html: response.result.value!)
        }
    }
    
    // MARK: - Image Getter
    
    func getImagesAndStoreIntoCache(guitar: Guitar)
    {
        let sharedImageCache: ImageCache = ImageCache.sharedImageCache

        let downloadQueue = dispatch_queue_create("suhrdownloadqueue",nil)
        
        dispatch_async(downloadQueue)
        {
            let cacheGuitarImage = CacheGuitarImage()
            
            // Get lo-res image.
            let loResImageData = NSData(contentsOfURL: NSURL(string: guitar.imageURL!)!)
            if (loResImageData != nil) {
                cacheGuitarImage.loResImage = UIImage(data: loResImageData!)
            }
            
            // Get hi-res image
            let hiResImageData = NSData(contentsOfURL: NSURL(string: guitar.largeImageURL!)!)
            if (hiResImageData != nil) {
                cacheGuitarImage.hiResImage = UIImage(data: hiResImageData!)
            }
            
            dispatch_async(dispatch_get_main_queue())
            {
                sharedImageCache.guitarImages[guitar.imageURL!] = cacheGuitarImage
            }
        }
    }
}


