//
//  HorizontalSeparator.swift
//  Suhr
//
//  Created by Marty Burolla on 5/2/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

class HorizontalSeparator : UIView
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
}