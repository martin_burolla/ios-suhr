//
//  CacheGuitarImage.swift
//  Suhr
//
//  Created by Marty Burolla on 5/6/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

class CacheGuitarImage
{
    var loResImage: UIImage?
    var hiResImage: UIImage?
}