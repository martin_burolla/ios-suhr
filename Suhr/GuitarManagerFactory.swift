//
//  GuitarManagerFactory.swift
//  Suhr
//
//  Created by Marty Burolla on 5/20/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation


class GuitarManagerFactory
{
    static func guitarManager() -> GuitarManager
    {
        #if DEBUG
            
            var environment = NSProcessInfo.processInfo().environment["env"] as! String
            
            switch environment
            {
                // Inject our dependencies
                case "suhr-prod":
                    return GuitarManager(htmlParserProtocol: HTMLParser(suhrProxyProtocol: SuhrProxy()),
                    userPreferenceProtocol: UserPreferenceProxy())
                case "local-dev":
                    return GuitarManager(htmlParserProtocol: MockHTMLParser_OneRealGuitar(suhrProxyProtocol: SuhrProxy()),
                    userPreferenceProtocol: MockUserPreferenceProxy())
                default:
                    return GuitarManager(htmlParserProtocol: HTMLParser(suhrProxyProtocol: SuhrProxy()),
                    userPreferenceProtocol: UserPreferenceProxy())
            }
            
        #else
            
            return GuitarManager(htmlParserProtocol: HTMLParser(suhrProxyProtocol: SuhrProxy()),
                userPreferenceProtocol: UserPreferenceProxy())
            
        #endif
    }
}