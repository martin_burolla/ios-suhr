//
//  FilterView.swift
//  Suhr
//
//  Created by Marty Burolla on 5/6/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import Snap

protocol FilterViewDelegate
{
    func didSelectGuitarModel(guitarModel: String)
}

class FilterView : UIView, UIPickerViewDataSource, UIPickerViewDelegate
{
    // MARK: - Data members
    
    let allSuhrModels: [String] = ["Modern", "Modern Carve Top", "Modern Antique", "Standard", "Standard Pro", "Classic", "Classic T", "Classic Pro", "Classic Antique", "Classic T Antique"]
 
    let topHorizontalSeparator = HorizontalSeparator()
    let filterSwitch = UISwitch()
    let filterLabel = UILabel()
    let middleHorizontalSeparator = HorizontalSeparator()
    let pickerView = UIPickerView()
    let bottomHorizontalSeparator = HorizontalSeparator()
    
    var userDefaultSettings: UserDefaultSettings?
    var userPreferenceProxy: UserPreferenceProxy = UserPreferenceProxy()
    
    var delegate: FilterViewDelegate! = nil
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.userDefaultSettings = self.userPreferenceProxy.readUserDefaults()
        self.setupUI()
        self.setupConstraints()
        
        self.selectPickerRow()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Setup UI
    
    private func selectPickerRow()
    {
        let index = self.allSuhrModels.indexOf((self.userDefaultSettings!.filterThisModel!))
        
        if index != nil {
            self.pickerView.selectRow(index!, inComponent: 0, animated: false)
        }
        else {
             self.pickerView.selectRow(0, inComponent: 0, animated: false)
        }
    }
    
    private func setupUI()
    {
        self.backgroundColor = .whiteColor()
        self.setupTopHorizontalSeparator()
        self.setupFilterLabel()
        self.setupFilterSwitch()
        self.setupMiddleHorizontalSeparator()
        self.setupPicker()
        self.setupBottomHorizontalSeparator()
    }
    
    private func setupTopHorizontalSeparator()
    {
        self.topHorizontalSeparator.backgroundColor = SuhrDesignGuide.separatorColor()
        self.addSubview(self.topHorizontalSeparator)
    }
    
    private func setupFilterLabel()
    {
        self.filterLabel.backgroundColor = .whiteColor()
        self.filterLabel.text = "Filter Guitar Models"
        self.addSubview(self.filterLabel)
    }
    
    private func setupFilterSwitch()
    {
        if self.userDefaultSettings?.isFilterOn == true {
            self.filterSwitch.setOn(true, animated: false)
        }
        else {
            self.filterSwitch.setOn(false, animated: false)
        }
        
        self.filterSwitch.backgroundColor = .whiteColor()
        self.filterSwitch.addTarget(self, action: "filterSwitchToggle:", forControlEvents: UIControlEvents.AllTouchEvents)
        self.addSubview(self.filterSwitch)
    }
    
    private func setupMiddleHorizontalSeparator()
    {
        self.middleHorizontalSeparator.backgroundColor = SuhrDesignGuide.extraLightGrayColor()
        self.addSubview(self.middleHorizontalSeparator)
    }
    
    private func setupPicker()
    {
        if self.userDefaultSettings?.isFilterOn == true {
            self.pickerView.alpha = 1
            self.pickerView.userInteractionEnabled = true
        }
        else {
            self.pickerView.alpha = 0.6
            self.pickerView.userInteractionEnabled = false
        }
        
        self.pickerView.backgroundColor = .whiteColor()
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.addSubview(self.pickerView)
    }
    
    private func setupBottomHorizontalSeparator()
    {
        self.bottomHorizontalSeparator.backgroundColor = SuhrDesignGuide.separatorColor()
        self.addSubview(self.bottomHorizontalSeparator)
    }
    
    // MARK: - Setup Constraints
    
    private func setupConstraints()
    {
        self.setupTopHorizontalSeparatorConstraints()
        self.setupFilterLabelConstraints()
        self.setupFilterSwitchConstraints()
        self.setupMiddleHorizontalSeparatorConstraints()
        self.setupPickerConstraints()
        self.setupBottomHorizontalSeparatorConstraints()
    }
    
    private func setupTopHorizontalSeparatorConstraints()
    {
        self.topHorizontalSeparator.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.height.equalTo(1)
        }
    }
    
    private func setupFilterLabelConstraints()
    {
        self.filterLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.topHorizontalSeparator).with.offset(11)
            make.leading.equalTo(self.snp_leading).with.offset(10)
        }
    }
    
    private func setupFilterSwitchConstraints()
    {
        self.filterSwitch.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top).with.offset(5)
            make.trailing.equalTo(self.snp_trailing).with.offset(-10)
        }
    }
    
    private func setupMiddleHorizontalSeparatorConstraints()
    {
        self.middleHorizontalSeparator.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top).with.offset(41)
            make.leading.equalTo(self.snp_leading).with.offset(10)
            make.trailing.equalTo(self.snp_trailing).with.offset(-8)
            make.height.equalTo(1)
        }
    }

    private func setupPickerConstraints()
    {
        self.pickerView.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(self.snp_leading).with.offset(10)
            make.trailing.equalTo(self.snp_trailing).with.offset(-8)
            make.top.equalTo(self.middleHorizontalSeparator.snp_bottom).with.offset(2)
        }
    }
    
    private func setupBottomHorizontalSeparatorConstraints()
    {
        self.bottomHorizontalSeparator.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(self.snp_bottom)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.height.equalTo(1)
        }
    }
    
    // MARK: - FilterSwitch
    
    func filterSwitchToggle(sender: UISwitch!)
    {
        if self.filterSwitch.on
        {
            self.pickerView.userInteractionEnabled = true
            self.pickerView.alpha = 1
            self.userDefaultSettings!.isFilterOn = true
            
            let filterThisModel = self.allSuhrModels[self.pickerView.selectedRowInComponent(0)]
            self.userDefaultSettings?.filterThisModel = filterThisModel
            self.delegate?.didSelectGuitarModel(filterThisModel)
            print("Filter guitar: \(self.userDefaultSettings?.filterThisModel)")
        }
        else
        {
            self.pickerView.userInteractionEnabled = false
            self.pickerView.alpha = 0.6
            self.userDefaultSettings!.isFilterOn = false
            self.userDefaultSettings!.filterThisModel = "None"
            self.delegate?.didSelectGuitarModel("None")
        }
        
        self.userPreferenceProxy.writeUserDefaults(self.userDefaultSettings!)
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.allSuhrModels.count
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return allSuhrModels[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.userDefaultSettings?.filterThisModel = self.allSuhrModels[row]
        self.userPreferenceProxy.writeUserDefaults(self.userDefaultSettings!)
        
        self.delegate?.didSelectGuitarModel(self.allSuhrModels[row])
        
        print("Filter guitar: \(self.userDefaultSettings?.filterThisModel)")
    }
}