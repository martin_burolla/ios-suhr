//
//  Suhr.swift
//  SuhrNewGuitarFeedParser
//
//  Created by Marty Burolla on 4/21/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import Snap

class HomeViewController: UIViewController,
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    UINavigationControllerDelegate,
    SuhrGuitarManagerDelegate
{
    // MARK: - Data members
    
    let guitarManager = GuitarManagerFactory.guitarManager()
    
    let progressBar: UIProgressView = UIProgressView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var collectionView: UICollectionView?
    let buttonRefresh: UIButton = UIButton(type: .System)
    let horizontalSepartor: HorizontalSeparator = HorizontalSeparator()
    
    var settingsViewController: SettingsViewController?
    var settingsPopOverController: UIPopoverController?
    var userPreferenceProxy: UserPreferenceProxy?
    let sharedImageCache: ImageCache = ImageCache.sharedImageCache
    
    let kGuitarCellReuseIdentifier = "GuitarCell"
    let kWidthOfGuitarCell: CGFloat = 230
    let kNumberOfGuitars: Float = 50.0
    let kViewControllerTitle: String = "Just Shipped"
    
    // MARK: - Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.userPreferenceProxy = UserPreferenceProxy()
        
        self.guitarManager.delegate = self
        self.guitarManager.getJustShippedGuitars()
        
        self.setupUI()
        self.setupConstraints()
    }
    
    // MARK: - SuhrGuitarManagerDelegate
    
    func didFinishGettingGuitars(suhrGuitarManager: GuitarManager)
    {
        self.navigationItem.rightBarButtonItem?.enabled = true
        self.buttonRefresh.enabled = true

        // Apply filter.
        let userDefaultSettings = self.userPreferenceProxy?.readUserDefaults()
        self.guitarManager.filterGuitars(userDefaultSettings!.filterThisModel!)
        
        self.collectionView?.reloadData()
        self.activityIndicator.stopAnimating()
        self.progressBar.tintColor = .whiteColor()
        self.progressBar.progress = 0
    }
    
    func didFinishGettingOneGuitar(suhrGuitarManager: GuitarManager, guitarCount: Int)
    {
        self.progressBar.progress = Float(guitarCount)/self.kNumberOfGuitars
        self.collectionView?.reloadData()
        print("Guitar count: \(guitarCount)")
    }
    
    // MARK: - Setup UI
    
    func setupUI()
    {
        self.view.backgroundColor = .whiteColor()
        self.title = kViewControllerTitle
        
        self.setupProgressBar()
        self.setupCollectionView()
        self.setupActivityIndicator()
        self.setupHorizontalSeparator()
        self.setupButton()
        self.setupNavigationBar()
    }
    
    func setupNavigationBar()
    {
        let barButtonItem = UIBarButtonItem(title: "Settings",
            style: UIBarButtonItemStyle.Plain,
            target: self, action: "barButtonSettingsAction:")
        
        self.navigationItem.rightBarButtonItem = barButtonItem
        self.navigationItem.rightBarButtonItem?.enabled = false
    }
    
    func setupProgressBar()
    {
        self.progressBar.progress = 0
        self.progressBar.trackTintColor = .whiteColor()
        self.progressBar.tintColor = SuhrDesignGuide.blueColor()
        self.view.addSubview(self.progressBar)
    }
    
    func setupCollectionView()
    {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        
        self.collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: flowLayout)
        self.collectionView?.backgroundColor = SuhrDesignGuide.collectionViewBackgroundColor()
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
        self.collectionView?.registerClass(GuitarCollectionViewCell.self, forCellWithReuseIdentifier: kGuitarCellReuseIdentifier)
        self.view.addSubview(self.collectionView!)
    }
    
    func setupHorizontalSeparator()
    {
        self.horizontalSepartor.backgroundColor = .lightGrayColor()
        self.view.addSubview(self.horizontalSepartor)
    }
    
    func setupButton()
    {
        self.buttonRefresh.enabled = false
        self.buttonRefresh.backgroundColor = SuhrDesignGuide.buttonColor()
        self.buttonRefresh.setTitle("Refresh", forState: .Normal)
        self.buttonRefresh.tintColor = UIColor.whiteColor()
        self.buttonRefresh.addTarget(self, action: "buttonRefreshAction:", forControlEvents: .TouchUpInside)
        self.view.addSubview(self.buttonRefresh)
    }
    
    func setupActivityIndicator()
    {
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        self.activityIndicator.color = .blackColor()
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)
    }
    
    // MARK: - Setup Constraints
    
    func setupConstraints()
    {
        self.setupConstraintsProgressBar()
        self.setupConstraintsCollectionView()
        self.setupConstraintsActivityView()
        self.setupConstraintsHorizontalSeparator()
        self.setupConstraintsRefreshButton()
    }
    
    func setupConstraintsProgressBar()
    {
        self.progressBar.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.collectionView!.snp_bottom).with.offset(0)
            make.height.equalTo(2)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
        }
    }
    
    func setupConstraintsCollectionView()
    {
        self.collectionView!.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.view.snp_top).with.offset(SuhrDesignGuide.kHeightOfNavigationBar)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.bottom.equalTo(self.view.snp_bottom).with.offset(-55)
        })
    }
    
    func setupConstraintsHorizontalSeparator()
    {
        self.horizontalSepartor.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.collectionView!.snp_bottom)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.height.equalTo(0.5)
        }
    }
    
    func setupConstraintsRefreshButton()
    {
        self.buttonRefresh.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.horizontalSepartor.snp_bottom).with.offset(5)
            make.leading.equalTo(self.view.snp_leading).with.offset(5)
            make.trailing.equalTo(self.view.snp_trailing).with.offset(-5)
            make.bottom.equalTo(self.view.snp_bottom).with.offset(-5)
        }
    }
    
    func setupConstraintsActivityView()
    {
        self.activityIndicator.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.view.snp_top)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.bottom.equalTo(self.collectionView!.snp_bottom)
        })
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.guitarManager.guitarsToDisplay.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let guitarCell = self.collectionView!.dequeueReusableCellWithReuseIdentifier(kGuitarCellReuseIdentifier, forIndexPath: indexPath) as! GuitarCollectionViewCell
        
        guitarCell.modelLabel.text = self.guitarManager.guitarsToDisplay[indexPath.row].model
        guitarCell.neckRadiusLabel.text = self.guitarManager.guitarsToDisplay[indexPath.row].neckProfile
        guitarCell.showNewLabel = self.guitarManager.guitarsToDisplay[indexPath.row].isNew
        guitarCell.guitarView.image = self.sharedImageCache.guitarImages[self.guitarManager.guitarsToDisplay[indexPath.row].imageURL!]?.loResImage
        
        guitarCell.draw()
        
        return guitarCell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let trans = CATransition()
        trans.duration = 0.1
        trans.type = kCATransitionFade
        
        self.view.window?.layer.addAnimation(trans, forKey: kCATransitionFade)
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            let guitarInfoViewControlleriPhone = GuitarInfoViewControlleriPhone()
            guitarInfoViewControlleriPhone.guitar = self.guitarManager.guitarsToDisplay[indexPath.row]
            
            self.presentViewController(guitarInfoViewControlleriPhone, animated: false) { () -> Void in }
        }
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Pad)
        {
            let guitarInfoViewControlleriPad = GuitarInfoViewControlleriPhone()
            guitarInfoViewControlleriPad.guitar = self.guitarManager.guitarsToDisplay[indexPath.row]
            
            self.presentViewController(guitarInfoViewControlleriPad, animated: false) { () -> Void in }
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        var sizeOfGuitarCell: CGSize! = nil
        let totalWidthOfAllGuitars = kWidthOfGuitarCell * CGFloat(self.guitarManager.guitarsToDisplay.count)
        
        if totalWidthOfAllGuitars < self.view.frame.size.width
        {
            var newWidthOfGuitarCell: CGFloat = 0.0
            
            if (UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
                newWidthOfGuitarCell =  self.view.frame.size.width
            }
          
            if (UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                newWidthOfGuitarCell = self.view.frame.size.width / CGFloat(self.guitarManager.guitarsToDisplay.count)
            }
            
            sizeOfGuitarCell = CGSizeMake(newWidthOfGuitarCell, self.collectionView!.frame.height)
        }
        else {
            sizeOfGuitarCell = CGSizeMake(kWidthOfGuitarCell, self.collectionView!.frame.height)
        }
        
        return sizeOfGuitarCell
    }
    
    // MARK: - Button Actions
    
    func buttonRefreshAction(sender:UIButton!)
    {
        self.navigationItem.rightBarButtonItem?.enabled = false
        self.buttonRefresh.enabled = false
        
        self.progressBar.tintColor = SuhrDesignGuide.blueColor()
        self.activityIndicator.startAnimating()
        self.guitarManager.getJustShippedGuitars()
    }
    
    func barButtonSettingsAction(sender: UIBarButtonItem!)
    {
        self.navigationController?.delegate = self
        self.settingsViewController = SettingsViewController()
        self.settingsViewController!.homeViewController = self
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            self.navigationController?.pushViewController(settingsViewController!, animated: true)
        }
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Pad)
        {
            self.settingsPopOverController = UIPopoverController(contentViewController: settingsViewController!)
            self.settingsPopOverController?.popoverContentSize = CGSizeMake(300,330)
            self.settingsPopOverController?.presentPopoverFromBarButtonItem(sender, permittedArrowDirections: .Any, animated: true)
        }
    }
    
    // MARK: - UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, didShowViewController viewController: UIViewController, animated: Bool)
    {
        if viewController.title == kViewControllerTitle // TODO: Use class type.
        {
            let userDefaultSettings = self.userPreferenceProxy?.readUserDefaults()
            self.guitarManager.filterGuitars(userDefaultSettings!.filterThisModel!)
            self.collectionView?.reloadData()
        }
    }
}