//
//  SuhrStyleSheet.swift
//  Suhr
//
//  Created by Marty Burolla on 5/2/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

public class SuhrDesignGuide
{
    // MARK: - Data members
    
    static var color : UIColor?
    static let kHeightOfNavigationBar: CGFloat = 65.0
    
    // MARK: - Design methods
    
    static func separatorColor() -> UIColor
    {
        self.color = UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
        return self.color!
    }
    
    static func extraLightGrayColor() -> UIColor
    {
        self.color = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        return self.color!
    }
    
    static func blueColor() -> UIColor
    {
        self.color = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        return self.color!
    }
    
    static func buttonColor() -> UIColor
    {
        return .grayColor()
    }
    
    static func collectionViewBackgroundColor() -> UIColor
    {
        self.color = UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
        return self.color!
    }
    
    static func dealerFont() -> UIFont
    {
        return UIFont(name: "Helvetica-Bold", size: 18.0)!
    }
}