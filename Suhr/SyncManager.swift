//
//  SyncManager.swift
//  Suhr
//
//  Created by Marty Burolla on 5/10/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class SyncManager
{
    static func needToSyncServerSideGuitarsWithLocalGuitars(localGuitars: [Guitar], serverSideGuitars:[Guitar]) -> Bool
    {
        var needSync = false
        
        if (localGuitars.count == 0) {
            needSync = true
        }
        else if (serverSideGuitars[0].imageURL != localGuitars[0].imageURL) {
            needSync = true
        }
        
        return needSync
    }
}