//
//  GuitarInfoViewControlleriPad.swift
//  Suhr
//
//  Created by Marty Burolla on 5/4/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import Snap

class GuitarInfoViewControlleriPad : UIViewController
{
    // MARK: - Data members
    
    var guitar : Guitar?
    
    let dealerLabel = UIButton(type: .System)
    let msrpLabel = UILabel()
    let buttonDone : UIButton = UIButton(type: .System)
    
    // MARK: - Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = .whiteColor()
        
        self.setupUI()
        self.setupConstraints()
    }
    
    // MARK: - Setup UI
    
    private func setupUI()
    {
        self.setupDoneButton()
        self.setupDealerLabel()
        self.setupMSRPLabel()
    }
    
    private func setupDealerLabel()
    {
        self.dealerLabel.backgroundColor = .lightGrayColor()
        self.dealerLabel.backgroundColor = UIColor.lightGrayColor()
        self.dealerLabel.setTitle(guitar?.dealer?.name, forState: .Normal)
        self.dealerLabel.tintColor = UIColor.whiteColor()
        self.dealerLabel.addTarget(self, action: "buttonDealerAction:", forControlEvents: .TouchUpInside)
        self.view.addSubview(self.dealerLabel)
    }
    
    private func setupMSRPLabel()
    {
        self.msrpLabel.backgroundColor = .lightGrayColor()
        self.msrpLabel.text =  "$" + guitar!.msrp!
        self.msrpLabel.textColor = .whiteColor()
        self.msrpLabel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(self.msrpLabel)
    }
    
    private func setupDoneButton()
    {
        self.buttonDone.backgroundColor = SuhrDesignGuide.buttonColor()
        self.buttonDone.setTitle("Done", forState: .Normal)
        self.buttonDone.tintColor = .whiteColor()
        self.buttonDone.addTarget(self, action: "buttonDoneAction:", forControlEvents: .TouchUpInside)
        self.view.addSubview(self.buttonDone)
    }
    
    // MARK: - Setup Constraints
    
    private func setupConstraints()
    {
        self.setupDealerLabelConstraints()
        self.setupMSRPLabelConstraints()
        self.setupDoneButtonConstraints()
    }
    
    private func setupDealerLabelConstraints()
    {
        self.dealerLabel.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.view.snp_centerY).with.offset(-75)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.height.equalTo(50)
        })
    }
    
    private func setupMSRPLabelConstraints()
    {
        self.msrpLabel.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.dealerLabel.snp_bottom).with.offset(5)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.height.equalTo(50)
        })
    }
    
    private func setupDoneButtonConstraints()
    {
        self.buttonDone.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(self.view.snp_leading).with.offset(5)
            make.trailing.equalTo(self.view.snp_trailing).with.offset(-5)
            make.bottom.equalTo(self.view.snp_bottom).with.offset(-5)
            make.height.equalTo(45)
        }
    }
    
    // MARK: - Button Actions
    
    func buttonDoneAction(sender:UIButton!)
    {
        let trans = CATransition()
        trans.duration = 0.5
        trans.type = kCATransitionFade
        
        self.view.window?.layer.addAnimation(trans, forKey: kCATransitionFade)
        
        self.dismissViewControllerAnimated(false) { () -> Void in
            print("Dismiss popup.")
        }
    }
    
    func buttonDealerAction(sender:UIButton!)
    {
        print("Open URL: \(self.guitar!.dealer!.url)")
        let url: NSURL = NSURL(string: self.guitar!.dealer!.url)!
        UIApplication.sharedApplication().openURL(url)
    }
}