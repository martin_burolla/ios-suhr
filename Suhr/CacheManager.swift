//
//  CacheManager.swift
//  Suhr
//
//  Created by Marty Burolla on 5/21/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class CacheManager
{
    // Mark: - Data members
    
    let sharedImageCache: ImageCache = ImageCache.sharedImageCache
    let suhrProxyProtocol: SuhrProxyProtocol?
    
    // MARK: - Constructors
    
    init(suhrProxyProtocol: SuhrProxyProtocol)
    {
        self.suhrProxyProtocol = suhrProxyProtocol
    }
    
    // Mark: - Methods
    
    func cacheNewGuitarImageFromWebServer(guitar: Guitar)
    {
        let guitarImageFromCache = self.sharedImageCache.guitarImages[guitar.imageURL!]
        
        if(guitarImageFromCache == nil)
        {
            self.suhrProxyProtocol!.getImagesAndStoreIntoCache(guitar)
        }
    }
}


