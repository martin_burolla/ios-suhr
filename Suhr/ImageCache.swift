//
//  ImageCache.swift
//  Suhr
//
//  Created by Marty Burolla on 4/27/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import UIKit

class ImageCache
{
    // MARK: - Data members
    
    var guitarImages = [String : CacheGuitarImage]()
    
    // MARK: - Singleton
    
    static let sharedImageCache = ImageCache()
}
