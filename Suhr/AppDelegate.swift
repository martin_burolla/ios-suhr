//
//  AppDelegate.swift
//  Suhr
//
//  Created by Marty Burolla on 4/28/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
//import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        
        //Parse.setApplicationId(kParseAppID, clientKey: kParseClientKey)
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let rootViewController = HomeViewController()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        self.initializeUserDefaultsForFirstTime()
        //self.registerForPushNotifications(application)
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        
     
        return true
    }
    
    // MARK: - User Defaults 
    
    private func initializeUserDefaultsForFirstTime()
    {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        let data = nsUserDefaults.objectForKey("UserDefaults") as? NSData
        
        if data == nil
        {
            let userDefaultSettings = UserDefaultSettings()
            let userPreferenceProxy: UserPreferenceProxy = UserPreferenceProxy()
            userPreferenceProxy.writeUserDefaults(userDefaultSettings)
        }
    }
    
    // MARK: - Push Notifications
    
//    private func registerForPushNotifications(application: UIApplication)
//    {
//        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
//        
//        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
//        application.registerUserNotificationSettings(settings)
//        application.registerForRemoteNotifications()
//    }
//    
//    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
//    {
//        let installation = PFInstallation.currentInstallation()
//        installation.setDeviceTokenFromData(deviceToken)
//        
//        installation.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError?) -> Void in
//            print("Saved Apple Push notification token.")
//        }
//    }
//    
//    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
//    {
//       PFPush.handlePush(userInfo)
//        
//       if application.applicationState == UIApplicationState.Inactive {
//            PFAnalytics.trackAppOpenedWithRemoteNotificationPayloadInBackground(userInfo, block: nil)
//        }
//    }
//    
//    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
//    {
//        if error.code == 3010 {
//            print("Push notifications are not supported in the iOS Simulator.")
//        } else {
//            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
//        }
//    }
//    
    // MARK: - Background fetch
    
    func application(application: UIApplication,
        performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        
        BackgroundFetchManager.performFetchWithCompletionHandler { (UIBackgroundFetchResult) -> Void in
            completionHandler(UIBackgroundFetchResult)
        }
    }
        
    // MARK: - Device orientations
    
    func supportedInterfaceOrientations() -> Int
    {
        switch UIDevice.currentDevice().userInterfaceIdiom
        {
            case .Phone:
                return Int(UIInterfaceOrientationMask.Portrait.rawValue)
            case .Pad:
                return Int(UIInterfaceOrientationMask.Landscape.rawValue)
            default:
                return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        }
    }
}

