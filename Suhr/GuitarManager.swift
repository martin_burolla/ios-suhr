//
//  SuhrGuitarManager.swift
//  Suhr
//
//  Created by Marty Burolla on 4/27/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

protocol SuhrGuitarManagerDelegate
{
    func didFinishGettingGuitars(suhrGuitarManager: GuitarManager)
    func didFinishGettingOneGuitar(suhrGuitarManager: GuitarManager, guitarCount: Int)
}

class GuitarManager : HTMLParserDelegate
{
    // MARK: - Data members
 
    var countOfNewGuitars = 0
    var guitarsToDisplay: [Guitar] = [] // All the guitars or a subset of guitars from the HTML Parser.
    let userPreferenceProtocol: UserPreferenceProtocol?
    let sharedImageCache: ImageCache = ImageCache.sharedImageCache
    var delegate: SuhrGuitarManagerDelegate! = nil
    
    private let htmlParserProtocol: HTMLParserProtocol?
    
    // MARK: - Lifecycle
    
    init(htmlParserProtocol: HTMLParserProtocol, userPreferenceProtocol: UserPreferenceProtocol)
    {
        self.userPreferenceProtocol = userPreferenceProtocol
        self.htmlParserProtocol = htmlParserProtocol
        self.htmlParserProtocol!.delegate = self
    }
    
    // MARK: - Methods
    
    func getJustShippedGuitars()
    {
        //self.guitarsToDisplay.removeAll(keepCapacity: true)
        self.htmlParserProtocol?.getJustShippedGuitarsAndCacheImages()
    }
    
    func filterGuitars(filterGuitarModel: String)
    {
        if(filterGuitarModel == "None") {
            self.guitarsToDisplay = self.htmlParserProtocol!.guitars
        }
        else
        {
            self.guitarsToDisplay = self.htmlParserProtocol!.guitars.filter { (guitar: Guitar) -> Bool in
                
                if filterGuitarModel == guitar.model! {
                    return true
                }
                else {
                    return false
                }
            }
        }
    }
    
    // MARK: - SuhrNewGuitarFeedParserDelegate
    
    func didFinishParsing(suhrNewGuitarFeedParser: HTMLParserProtocol)
    {
        self.guitarsToDisplay = self.htmlParserProtocol!.guitars
        self.markNewGuitars()
        self.delegate?.didFinishGettingGuitars(self)
        
        print("Guitar count: \(self.guitarsToDisplay.count)")
    }
    
    func didFinishParsingOneGuitar(suhrNewGuitarFeedParser: HTMLParserProtocol, guitarCount: Int)
    {
        self.guitarsToDisplay.append(suhrNewGuitarFeedParser.guitars[guitarCount-1])
        self.delegate?.didFinishGettingOneGuitar(self, guitarCount: guitarCount)
    }
    
    // MARK: - NEW label
    
    private func markNewGuitars()
    {
        self.countOfNewGuitars = 0
        var noMoreNewGuitars : Bool = false
        let userDefaultSettings = self.userPreferenceProtocol!.readUserDefaults()
        
        for guitar in self.guitarsToDisplay
        {
            if(guitar.imageURL != userDefaultSettings.lastViewedGuitarImageURL && noMoreNewGuitars == false) {
                guitar.isNew = true
            }
            else {
                self.countOfNewGuitars++
                guitar.isNew = false
                noMoreNewGuitars = true
            }
        }
        
        // Save the URL of the newest guitar, to determine if the NEW label should be displayed on unseen guitars.
        userDefaultSettings.lastViewedGuitarImageURL = self.guitarsToDisplay[0].imageURL
        self.userPreferenceProtocol!.writeUserDefaults(userDefaultSettings)
    }
}