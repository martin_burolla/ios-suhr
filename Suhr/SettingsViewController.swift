//
//  Settings.swift
//  Suhr
//
//  Created by Marty Burolla on 5/4/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import Snap

class SettingsViewController : UIViewController, FilterViewDelegate
{
    // MARK: - Data members
    
    let kUnchangeableHeight: Float = 266.0
    let buttonDone: UIButton = UIButton(type: .System)
    var homeViewController: HomeViewController?

    let filterView = FilterView()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupConstraints()
        
    }
    
    // MARK: - Setup UI
    
    func setupUI()
    {
        self.view.backgroundColor = SuhrDesignGuide.extraLightGrayColor()
        self.setupFilterView()
    }

    private func setupFilterView()
    {
        self.filterView.delegate = self
        self.view.addSubview(self.filterView)
    }
    
    // MARK: - Setup Constraints
    
    func setupConstraints()
    {
        self.setupFilterViewConstraints()
    }

    private func setupFilterViewConstraints()
    {
        var iPhoneOffset: CGFloat = 35
        
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
            iPhoneOffset = SuhrDesignGuide.kHeightOfNavigationBar+35
        }
        
        self.filterView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.view.snp_top).with.offset(iPhoneOffset)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.height.equalTo(self.kUnchangeableHeight)
        }
    }
    
    // MARK: - FilterViewDelegate
    
    func didSelectGuitarModel(guitarModel: String)
    {
        homeViewController?.guitarManager.filterGuitars(guitarModel)
        homeViewController?.collectionView!.reloadData()
    }
}