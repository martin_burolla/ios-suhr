//
//  GuitarCollectionViewCell.swift
//  SuhrNewGuitarFeedParser
//
//  Created by Marty Burolla on 4/22/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation
import Snap

class GuitarCollectionViewCell : UICollectionViewCell
{
    // MARK: - Data members
    
    let guitarView = UIImageView()
    let modelLabel = UILabel()
    let neckRadiusLabel = UILabel()
    let newLabel = UILabel()
    var showNewLabel : Bool = false
    
    // MARK: - Construction
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    func draw()
    {
        self.setupUI()
        self.setupConstraints()
    }
    
    // MARK: - User Interface
    
    func setupUI()
    {
        self.backgroundColor = .whiteColor()
        
        self.guitarView.contentMode = UIViewContentMode.ScaleAspectFit
        self.addSubview(self.guitarView)
        
        self.modelLabel.backgroundColor = .whiteColor()
        self.modelLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(self.modelLabel)
        
        self.neckRadiusLabel.backgroundColor = .whiteColor()
        self.neckRadiusLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(self.neckRadiusLabel)
        
        if(self.showNewLabel)
        {
            self.newLabel.backgroundColor = .redColor()
            self.newLabel.textColor = .whiteColor()
            self.newLabel.textAlignment = NSTextAlignment.Center
            self.newLabel.text = "NEW"
        
            self.addSubview(self.newLabel)
        }
        else
        {
            self.newLabel.removeFromSuperview()
        }
    }
    
    func setupConstraints()
    {
        self.guitarView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.bottom.equalTo(self.modelLabel.snp_top)
        }
        
        self.modelLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.bottom.equalTo(self.neckRadiusLabel.snp_top)
        }
        
        self.neckRadiusLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.bottom.equalTo(self.snp_bottom)
        }
        
        if(self.showNewLabel)
        {
            self.newLabel.snp_makeConstraints { (make) -> Void in
                make.top.equalTo(self.snp_top)
                make.leading.equalTo(self.snp_leading)
                make.width.equalTo(60)
                make.height.equalTo(30)
            }
        }
    }
}