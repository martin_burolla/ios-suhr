//
//  UserPreferenceProtocol.swift
//  Suhr
//
//  Created by Marty Burolla on 5/7/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

protocol UserPreferenceProtocol
{
     func readUserDefaults() -> UserDefaultSettings
     func writeUserDefaults(userDefaults: UserDefaultSettings)
}

class UserPreferenceProxy : UserPreferenceProtocol
{
    let kUserDefaultsKey = "UserDefaults"
    
    func readUserDefaults() -> UserDefaultSettings
    {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        let data = nsUserDefaults.objectForKey(kUserDefaultsKey) as? NSData
        let unarc = NSKeyedUnarchiver(forReadingWithData: data!)
        
        return unarc.decodeObjectForKey("root") as! UserDefaultSettings
    }
    
    func writeUserDefaults(userDefaults: UserDefaultSettings)
    {
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        
        nsUserDefaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(userDefaults),
            forKey: kUserDefaultsKey)
    }
}