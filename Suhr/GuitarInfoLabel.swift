////
////  GuitarInfoLabel.swift
////  Suhr
////
////  Created by Marty Burolla on 5/7/15.
////  Copyright (c) 2015 Marty Burolla. All rights reserved.
////

import UIKit
import Snap

class GuitarInfoLabel : UIView
{
    // MARK: - Data members
    
    let dealerButton = UIButton(type: .System)
    let msrpLabel = UILabel()
    var dealerURL: String?
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.setupUI()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Setup UI
    
    private func setupUI()
    {
        self.backgroundColor = .grayColor()
        self.setupDealerButtton()
        self.setupMSRPLabel()
    }
    
    private func setupDealerButtton()
    {
        self.dealerButton.backgroundColor = .whiteColor()
        self.dealerButton.tintColor = .blackColor()
        self.dealerButton.titleLabel?.font = SuhrDesignGuide.dealerFont()
        self.dealerButton.addTarget(self, action: "buttonDealerAction:", forControlEvents: .TouchUpInside)
        self.addSubview(self.dealerButton)
    }
    
    private func setupMSRPLabel()
    {
        self.msrpLabel.backgroundColor = .whiteColor()
        self.msrpLabel.textColor = .blackColor()
        self.msrpLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(self.msrpLabel)
    }
    
    // MARK: - Setup Constraints
    
    private func setupConstraints()
    {
        self.setupDealerButtonConstraints()
        self.setupMSRPLabelConstraints()
    }
    
    private func setupDealerButtonConstraints()
    {
        self.dealerButton.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.snp_top).with.offset(0.5)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.height.equalTo(30)
        })
    }
    
    private func setupMSRPLabelConstraints()
    {
        self.msrpLabel.snp_makeConstraints({ (make) -> Void in
            make.top.equalTo(self.dealerButton.snp_bottom)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.bottom.equalTo(self.snp_bottom).with.offset(-0.5)
        })
    }
    
    // MARK: - Button Actions
    
    func buttonDealerAction(sender:UIButton!)
    {
        print("Dealer URL: \(self.dealerURL)")
        
        let url: NSURL = NSURL(string: self.dealerURL!)!
        UIApplication.sharedApplication().openURL(url)
    }
}