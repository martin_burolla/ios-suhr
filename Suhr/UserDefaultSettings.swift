//
//  UserDefaultSettings.swift
//  Suhr
//
//  Created by Marty Burolla on 5/6/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

// The one and only object we are storing in NSUserDefaults, key = "UserDefaults"

class UserDefaultSettings : NSObject, NSCoding
{
    // MARK: - Data members

    var lastViewedGuitarImageURL: String? = ""
    var isFilterOn: Bool? = false
    var filterThisModel: String? = "None"
    
    // MARK: - Serialization
    
    override init() {}
    
    required init?(coder aDecoder: NSCoder)
    {
        if let lastGuitarImageURL = aDecoder.decodeObjectForKey("lastGuitarImageURL") as? String {
            self.lastViewedGuitarImageURL = lastGuitarImageURL
        }
        
        if let isFilterOn = aDecoder.decodeObjectForKey("isFilterOn") as? Bool {
            self.isFilterOn = isFilterOn
        }
        
        if let filterGuitarModelList = aDecoder.decodeObjectForKey("filterThisModel") as? String {
            self.filterThisModel = filterGuitarModelList
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        if let lastGuitarImageURL = self.lastViewedGuitarImageURL {
            aCoder.encodeObject(lastGuitarImageURL, forKey: "lastGuitarImageURL")
        }
        
        if let isFilterOn = self.isFilterOn {
            aCoder.encodeObject(isFilterOn, forKey: "isFilterOn")
        }
        
        if let filterThisModel = self.filterThisModel {
            aCoder.encodeObject(filterThisModel, forKey: "filterThisModel")
        }
    }
}