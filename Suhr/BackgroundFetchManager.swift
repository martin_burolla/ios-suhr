//
//  FetchNotificationManager.swift
//  Suhr
//
//  Created by Marty Burolla on 5/21/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit

class BackgroundFetchManager
{
    static func performFetchWithCompletionHandler(completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        // NOTE: This method will not be invoked when using the iOS simulator in Xcode 6.3.1.
        // You MUST connect a real iOS device to test this method.
        
        let guitarManager = GuitarManager(htmlParserProtocol: HTMLParser(suhrProxyProtocol: SuhrProxy()), userPreferenceProtocol: UserPreferenceProxy())
        
        if (guitarManager.countOfNewGuitars > 0) {
            completionHandler(UIBackgroundFetchResult.NewData)
            self.displayLocalNotification(guitarManager.countOfNewGuitars)
        }
        else {
            completionHandler(UIBackgroundFetchResult.NoData)
        }
    }
    
    static func displayLocalNotification(numGuitars: Int)
    {
        let uiLocalNotification = UILocalNotification();
        uiLocalNotification.fireDate = NSDate().dateByAddingTimeInterval(5)
        uiLocalNotification.alertBody = "Shipped \(numGuitars) new guitars."
        uiLocalNotification.timeZone = NSTimeZone.defaultTimeZone()
        UIApplication.sharedApplication().scheduleLocalNotification(uiLocalNotification)
    }
}