//
//  GuitarInfoViewControlleriPhone.swift
//  Suhr
//
//  Created by Marty Burolla on 4/29/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import UIKit
import Snap

class GuitarInfoViewControlleriPhone : UIViewController
{
    // MARK: - Data members
    
    var guitar : Guitar?
    
    let guitarInfoLabel = GuitarInfoLabel()
    let backgroundImage = UIImageView()
    
    let sharedImageCache: ImageCache = ImageCache.sharedImageCache
    
    // MARK: - Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = .grayColor()
        
        self.setupUI()
        self.setupConstraints()
    }
    
    // MARK: - Setup UI
    
    private func setupUI()
    {
        self.setupBackgroundImage()
        self.setGuitarInfoLabel()
    }
    
    private func setGuitarInfoLabel()
    {
        if ((guitar!.msrp!).characters.count > 0) {
            self.guitarInfoLabel.msrpLabel.text = "MSRP: $" + guitar!.msrp!
        }
        
        self.guitarInfoLabel.dealerButton.setTitle(guitar?.dealer?.name, forState: .Normal)
        self.guitarInfoLabel.dealerURL = guitar!.dealer?.url
        self.view.addSubview(self.guitarInfoLabel)
    }
    
    private func setupBackgroundImage()
    {
        self.backgroundImage.backgroundColor = .whiteColor()
        self.backgroundImage.image = self.sharedImageCache.guitarImages[guitar!.imageURL!]?.hiResImage
        self.backgroundImage.contentMode = UIViewContentMode.ScaleAspectFit
        self.view.addSubview(self.backgroundImage)
    }
    
    // MARK: - Setup Constraints
    
    private func setupConstraints()
    {
        self.setupBackgroundImageConstraints()
        self.setupGuitarInfoLabelConstraints()
    }
    
    private func setupGuitarInfoLabelConstraints()
    {
        self.guitarInfoLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.view.snp_centerY).width.offset(20)
            make.leading.equalTo(self.view.snp_leading)
            make.trailing.equalTo(self.view.snp_trailing)
            make.height.equalTo(60)
        }
    }
    
    private func setupBackgroundImageConstraints()
    {
        self.backgroundImage.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.view.snp_top).with.offset(-700)
            make.leading.equalTo(self.view.snp_leading).with.offset(-100)
            make.trailing.equalTo(self.view.snp_trailing).with.offset(100)
            make.bottom.equalTo(self.view.snp_bottom).with.offset(100)
        }
    }
    
    // MARK: - UIGestureRecognizer
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        let trans = CATransition()
        trans.duration = 0.5
        trans.type = kCATransitionFade
        self.view.window?.layer.addAnimation(trans, forKey: kCATransitionFade)
        self.dismissViewControllerAnimated(false) { () -> Void in }
    }
}