//
//  Guitar.swift
//  SuhrNewGuitarFeedParser
//
//  Created by Marty Burolla on 4/22/15.
//  Copyright (c) 2015 Marty Burolla. All rights reserved.
//

import Foundation

class Guitar
{
    var model: String?
    var neckProfile: String?
    
    var detailURL: String? // Spec sheet
    var imageURL: String?
    var largeImageURL: String?

    var isNew: Bool = false // Has the user seen this guitar before?
    var dealer: Dealer?
    var msrp: String?
}